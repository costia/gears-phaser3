"use strict";
const webpack = require("webpack");
const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CopyWebpackPlugin = require("copy-webpack-plugin");
var BundleAnalyzerPlugin = require("webpack-bundle-analyzer")
  .BundleAnalyzerPlugin;

module.exports = {
  mode: "development", //production
  entry: "./src/index.js",
  output: {
    path: path.resolve(__dirname, "build"),
    filename: "[name].[chunkhash].js",
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        include: path.join(__dirname, "src"),
        loader: "babel-loader",
      },
    ],
  },

  plugins: [
    new HtmlWebpackPlugin({
      path: path.resolve(__dirname, "build", "index.html"),
      template: "./static/index.html",
    }),
    new CopyWebpackPlugin([
      {
        from: path.resolve(__dirname, "assets"),
        to: path.resolve(__dirname, "build", "assets"),
      },
    ]),
    //new BundleAnalyzerPlugin(),
  ],
};
