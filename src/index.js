import phaser from "phaser";
import { BallSource } from "./GameObjects/BallSource";
import { Ball } from "./GameObjects/Ball";
import { BallSink } from "./GameObjects/BallSink";
import { Gear } from "./GameObjects/Gear";
import { ScoreBoard } from "./GameObjects/ScoreBoard";
import { GameInfo } from "./GameObjects/GameInfo";
import { Tunnel } from "./GameObjects/Tunnel";
import { GearMover } from "./GameObjects/GearMover";
import { GapFiller } from "./GameObjects/GapFiller";

export class Gears extends phaser.Scene {
  constructor() {
    super({
      key: "gears",
    });
  }

  loadLevel(url) {
    for (let ind in this.allObjDict) {
      this.allObjDict[ind].destroy();
    }
    this.allObjDict = {};
    let xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);
    xhr.responseType = "json";
    xhr.onload = () => {
      let status = xhr.status;
      if (status === 200) {
        this.objData = xhr.response;
        for (let ind in this.objData) {
          this.objData[ind].type = this.objData[ind].type.toLowerCase();
          if (this.objData[ind].type == "gear") {
            new Gear(this, this.objData[ind]);
          }
          if (this.objData[ind].type == "tunnel") {
            new Tunnel(this, this.objData[ind]);
          }
          if (this.objData[ind].type == "ballsource") {
            new BallSource(this, this.objData[ind]);
          }
          if (this.objData[ind].type == "ball") {
            new Ball(this, this.objData[ind]);
          }
          if (this.objData[ind].type == "ballsink") {
            new BallSink(this, this.objData[ind]);
          }
          if (this.objData[ind].type == "gearmover") {
            new GearMover(this, this.objData[ind]);
          }
          if (this.objData[ind].type == "gapfiller") {
            new GapFiller(this, this.objData[ind]);
          }
        }
        // new ScoreBoard(this, { type: "scoreboard", ID: -2 });

        new GameInfo(this, { type: "gameinfo", ID: -1 });
        this.Timer = new Date().getTime();
        this.gameEnded = false;
        this.EndTime = null;
        this.Moves = 0;

        // first "update" iteration
        // without "getStateChange" check
        // to settle initial state
        for (let ind in this.allObjDict) {
          this.allObjDict[ind].updateGameobjectState();
        }
        for (let ind in this.allObjDict) {
          this.checkSend(this.allObjDict[ind]);
          this.checkReceive(this.allObjDict[ind]);
        }
        for (let ind in this.allObjDict) {
          this.allObjDict[ind].updateRenderedState();
        }
      } else {
        alert("loading failed");
      }
    };
    xhr.send();
  }

  preload() {
    this.load.audio("gearClick", "assets/gearClick.mp3", {
      instances: 1,
    });
    this.load.audio("ballFall", "assets/ballFall.mp3", {
      instances: 1,
    });
    this.load.audio("gearMove", "assets/gearMove.mp3", {
      instances: 1,
    });
    this.load.audio("ballFire", "assets/ballFire.mp3", {
      instances: 1,
    });
    this.load.image("fireSprite", "assets/fire.png");
  }

  create() {
    this.gameEnded = true;
    this.Timer = new Date().getTime();
    this.Moves = 0;

    this.cameras.main.setBackgroundColor("rgba(233, 233, 210, 1)");

    this.loadLevel("assets/level1.json");
  }

  formatTimeMs(timeInMS) {
    let hSeconds = Math.floor(timeInMS / 10) % 100;
    if (hSeconds < 10) {
      hSeconds = "0" + String(hSeconds);
    } else {
      hSeconds = String(hSeconds);
    }
    let seconds = Math.floor(timeInMS / 1000) % 60;
    if (seconds < 10) {
      seconds = "0" + String(seconds);
    } else {
      seconds = String(seconds);
    }
    let minutes = Math.floor(timeInMS / 1000 / 60);
    if (minutes < 10) {
      minutes = "0" + String(minutes);
    } else {
      minutes = String(minutes);
    }
    return minutes + ":" + seconds + ":" + hSeconds;
  }

  checkWinLose() {
    for (let ind in this.allObjDict) {
      let gameObj = this.allObjDict[ind];
      if (gameObj.getType() != "ballsink") continue;
      if (gameObj.isAnimating()) continue;
      if (gameObj.infoStruct.destroy) continue;
      if (gameObj.ballsList.length) {
        this.EndTime = new Date().getTime();
        this.gameEnded = true;
        let xhr = new XMLHttpRequest();
        xhr.open("POST", "https://gears-97629.firebaseio.com/.json", true);
        let data = {
          userID: Math.floor(Math.random() * 10000),
          moves: this.Moves,
          completeTime: this.EndTime - this.Timer,
          timestamp: new Date().getTime(),
        };
        xhr.send(JSON.stringify(data));
        new ScoreBoard(this, { type: "scoreboard", ID: -2 });
      }
    }
  }
  update() {
    if (this.gameEnded) return;
    for (let ind in this.allObjDict) {
      this.allObjDict[ind].updateGameobjectState();
    }
    for (let ind in this.allObjDict) {
      if (this.allObjDict[ind].getStateChanged()) {
        // since checking only changed objects
        // must check both send and receive
        this.checkSend(this.allObjDict[ind]);
        this.checkReceive(this.allObjDict[ind]);
      }
    }

    this.checkWinLose();

    for (let ind in this.allObjDict) {
      if (this.allObjDict[ind].getStateChanged()) {
        this.allObjDict[ind].updateRenderedState();
      }
    }
  }

  checkSend(gameObj) {
    if (!gameObj.infoStruct.sendsTo) return;
    for (let ind = 0; ind < gameObj.infoStruct.sendsTo.length; ind++) {
      if (gameObj.infoStruct.sendsTo[ind] < 0) continue;
      let targetID = gameObj.infoStruct.sendsTo[ind];
      if (!gameObj.canSend(targetID)) continue;
      let target = gameObj.scene.allObjDict[targetID];
      if (target.canReceive(gameObj.infoStruct.ID)) {
        target.receiveFrom(gameObj.infoStruct.ID, gameObj.sendTo(targetID));
      }
    }
  }
  checkReceive(gameObj) {
    if (!gameObj.infoStruct.receivesFrom) return;
    for (let ind = 0; ind < gameObj.infoStruct.receivesFrom.length; ind++) {
      if (gameObj.infoStruct.receivesFrom[ind] < 0) continue;
      let targetID = gameObj.infoStruct.receivesFrom[ind];
      if (!gameObj.canReceive(targetID)) continue;

      let target = gameObj.scene.allObjDict[targetID];
      if (target.canSend(gameObj.infoStruct.ID)) {
        gameObj.receiveFrom(targetID, target.sendTo(gameObj.infoStruct.ID));
      }
    }
  }
}

const config = {
  width: 600,
  height: 600,
  parent: "content",
  scene: [Gears],
};
const game = new phaser.Game(config);
