import phaser from "phaser";

class gameObj {
  getType() {
    return this.infoStruct.type;
  }
  getID() {
    return this.infoStruct.ID;
  }
  constructor(scene, objInfo) {
    this.renderedObjects = [];

    this.infoStruct = { ...objInfo };
    this.infoStruct.type = this.infoStruct.type.toLowerCase();
    this.scene = scene;
    //scene.add.existing(this.renderedObject);
    scene.allObjDict[String(this.infoStruct.ID)] = this;
  }
  canReceive(fromID) {
    return false;
  }
  canSend(toID) {
    return false;
  }
  sendTo(toID) {}
  receiveFrom(fromID, objID) {}
  updateRenderedState() {}
  updateGameobjectState() {}
  destroy() {
    for (let ind in this.renderedObjects) {
      this.renderedObjects[ind].destroy();
    }
  }
  getStateChanged() {
    return true;
  }
  isAnimating() {
    return false;
  }
}

export { gameObj };
