import phaser from "phaser";

class BallSource {
  getType() {
    return this.infoStruct.type;
  }
  getID() {
    return this.infoStruct.ID;
  }
  constructor(scene, objInfo) {
    let source = new phaser.GameObjects.Graphics(scene);

    var rect = new Phaser.Geom.Rectangle(
      -objInfo.width / 2,
      -objInfo.height / 2,
      objInfo.width,
      objInfo.height
    );
    source.fillStyle(
      phaser.Display.Color.HexStringToColor(objInfo.color).color32,
      1
    );
    source.fillRectShape(rect);

    this.renderedObject = source;
    this.renderedObject.x = objInfo.location[0];
    this.renderedObject.y = objInfo.location[1];
    this.renderedObject.angle = objInfo.rotation;

    this.ballsList = [...objInfo.balls];
    this.stateChanged = false;

    this.infoStruct = { ...objInfo };
    this.infoStruct.type = this.infoStruct.type.toLowerCase();
    this.scene = scene;
    scene.add.existing(this.renderedObject);
    scene.allObjDict[String(this.infoStruct.ID)] = this;
  }
  updateRenderedState() {
    for (let ind in this.ballsList) {
      let ballID = this.ballsList[ind];
      let ballObj = this.scene.allObjDict[ballID];
      if (ballObj) {
        ballObj.animatePosition(
          this.infoStruct.location[0],
          this.infoStruct.location[1],
          ballObj.infoStruct.rotation
        );
      }
    }
    this.stateChanged = false;
  }
  canReceive(fromID) {
    return false;
  }
  canSend(toID) {
    let isInList = false;
    for (let ind in this.infoStruct.sendsTo) {
      if (toID == this.infoStruct.sendsTo[ind]) {
        isInList = true;
        break;
      }
    }
    return isInList && this.ballsList.length > 0;
  }
  sendTo(toID) {
    if (!this.canSend(toID)) {
      throw "source " +
        String(this.infoStruct.ID) +
        " cant send to" +
        String(toID);
    }
    this.stateChanged = true;
    let ballID = this.ballsList.pop();
    return ballID;
  }
  receiveFrom(fromID, objID) {
    throw "source obj can't receive items";
  }
  updateGameobjectState() {}
  destroy() {
    this.renderedObject.destroy();
  }
  getStateChanged() {
    return this.stateChanged;
  }
  isAnimating() {
    return false;
  }
}

export { BallSource };
