import { GlobalSettings } from "../Globals";

class ScoreBoard {
  getType() {
    return this.infoStruct.type;
  }
  getID() {
    return this.infoStruct.ID;
  }
  constructor(scene, objInfo) {
    this.scoreboardObj = [];
    let gfxRect = scene.add.graphics();
    var rect = new Phaser.Geom.Rectangle(0, 0, 600, 530);
    gfxRect.fillStyle(0xdddddd, 0.85);
    gfxRect.fillRectShape(rect);
    gfxRect.depth = 100;
    this.scoreboardObj.push(gfxRect);

    let loadingText = scene.add
      .text(300, 300, "Loading latest scores.", {
        fontFamily: GlobalSettings.Font,
        fill: "#000",
        fontSize: "16px",
        align: "center",
      })
      .setOrigin(0.5);
    loadingText.depth = 101;
    this.scoreboardObj.push(loadingText);

    let xhr = new XMLHttpRequest();
    xhr.open(
      "GET",
      'https://gears-97629.firebaseio.com/.json?orderBy="timestamp"&&limitToLast=3',
      true
    );
    xhr.onload = () => {
      let status = xhr.status;
      if (status === 200) {
        loadingText.text = "";
        this.latestResults = JSON.parse(xhr.response);

        let text = scene.add
          .text(300, 50, "Latest results", {
            fontFamily: GlobalSettings.Font,
            fill: "#000",
            fontSize: "40px",
            align: "center",
          })
          .setOrigin(0.5);
        text.depth = 101;
        this.scoreboardObj.push(text);

        let index = 0;
        for (let ind in this.latestResults) {
          let textString =
            "userID: " +
            this.latestResults[ind].userID +
            "\nmoves: " +
            this.latestResults[ind].moves +
            "\ntime: " +
            scene.formatTimeMs(this.latestResults[ind].completeTime);
          let text = scene.add
            .text(300, 170 + 120 * index, textString, {
              fontFamily: GlobalSettings.Font,
              fill: "#000",
              fontSize: "24px",
              align: "center",
            })
            .setOrigin(0.5);

          index++;
          text.depth = 101;
          this.scoreboardObj.push(text);
        }
      }
    };
    // let CountDownText = scene.add.text(150, 450, "Reloading in 3", {
    //   fontFamily: GlobalSettings.Font,
    //   fill: "#000",
    //   fontSize: "16px",
    // });
    // CountDownText.depth = 101;
    // this.scoreboardObj.push(CountDownText);

    // this.reloadCountDown = 2;
    // this.interval = setInterval(() => {
    //   if (this.reloadCountDown == 0) {
    //     clearInterval(this.interval);
    //     scene.loadLevel("assets/level1.json");
    //   } else {
    //     CountDownText.text = "Reloading in " + String(this.reloadCountDown);
    //   }
    //   this.reloadCountDown--;
    // }, 1000);
    xhr.send();

    this.infoStruct = { ...objInfo };
    this.infoStruct.type = this.infoStruct.type.toLowerCase();
    this.scene = scene;
    scene.allObjDict[String(this.infoStruct.ID)] = this;
  }
  canReceive(fromID) {
    return false;
  }
  canSend(toID) {
    return false;
  }
  sendTo(toID) {}
  receiveFrom(fromID, objID) {}
  updateRenderedState() {}
  updateGameobjectState() {}
  destroy() {
    for (let ind in this.scoreboardObj) {
      this.scoreboardObj[ind].destroy();
    }
  }
  getStateChanged() {
    return false;
  }
  isAnimating() {
    return false;
  }
}

export { ScoreBoard };
