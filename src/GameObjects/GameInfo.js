import phaser from "phaser";
import { GlobalSettings } from "../Globals";

class GameInfo {
  getType() {
    return this.infoStruct.type;
  }
  getID() {
    return this.infoStruct.ID;
  }
  constructor(scene, objInfo) {
    this.renderedObjects = [];
    let fillColor = phaser.Display.Color.HexStringToColor("0xcccca3");
    let textColor = fillColor.clone().darken(50);
    let gfxRect = scene.add.graphics();
    var rect = new Phaser.Geom.Rectangle(0, 530, 600, 70);
    gfxRect.fillStyle(fillColor.color32, 1);
    gfxRect.fillRectShape(rect);
    this.renderedObjects.push(gfxRect);

    this.movesCounterText = scene.add.text(10, 550, "Moves: 0", {
      fontSize: "32px",
      fill: textColor.rgba,
      fontFamily: GlobalSettings.Font,
    });

    this.renderedObjects.push(this.movesCounterText);

    this.movesTimerText = scene.add.text(420, 550, "00:00:00", {
      fontSize: "32px",
      fill: textColor.rgba,
      fontFamily: GlobalSettings.Font,
    });
    this.renderedObjects.push(this.movesTimerText);

    this.restartButton = scene.add.graphics();
    var ButtonRect = new Phaser.Geom.Rectangle(200, 545, 200, 45);
    let buttonColor = fillColor.clone().darken(20);
    this.restartButton.fillStyle(buttonColor.color32, 1);
    this.restartButton.fillRectShape(ButtonRect);
    this.restartButton.setInteractive(ButtonRect, (shape, x, y, obj) => {
      return phaser.Geom.Rectangle.Contains(shape, x, y);
    });

    this.restartButton.on("pointerdown", function(
      pointer,
      localX,
      localY,
      event
    ) {
      scene.loadLevel("assets/level1.json");
    });

    this.renderedObjects.push(this.restartButton);

    this.restartText = scene.add
      .text(300, 565, "RESTART", {
        fontSize: "32px",
        align: "center",
        fill: textColor.rgba,
        fontFamily: GlobalSettings.Font,
      })
      .setOrigin(0.5);
    this.renderedObjects.push(this.restartText);

    this.infoStruct = { ...objInfo };
    this.infoStruct.type = this.infoStruct.type.toLowerCase();
    this.scene = scene;
    //scene.add.existing(this.renderedObject);
    scene.allObjDict[String(this.infoStruct.ID)] = this;
  }
  canReceive(fromID) {
    return false;
  }
  canSend(toID) {
    return false;
  }
  sendTo(toID) {}
  receiveFrom(fromID, objID) {}
  updateRenderedState() {
    let timeToUse = new Date().getTime();
    if (this.scene.gameEnded) {
      timeToUse = this.scene.EndTime;
    }

    let diff = timeToUse - this.scene.Timer;

    this.movesTimerText.text = this.scene.formatTimeMs(diff);
    this.movesCounterText.text =
      "Moves: " + String(Math.ceil(this.scene.Moves));
  }
  updateGameobjectState() {}
  destroy() {
    for (let ind in this.renderedObjects) {
      this.renderedObjects[ind].destroy();
    }
    //this.renderedObject.destroy();
  }
  getStateChanged() {
    return true;
  }
  isAnimating() {
    return false;
  }
}

export { GameInfo };
