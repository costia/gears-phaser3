import phaser from "phaser";

class GapFiller {
  getType() {
    return this.infoStruct.type;
  }
  getID() {
    return this.infoStruct.ID;
  }
  constructor(scene, objInfo) {
    this.renderedObjects = [];

    let triangle = new phaser.Geom.Triangle(
      -objInfo.size,
      0,
      objInfo.size * 2,
      objInfo.size,
      objInfo.size * 2,
      -objInfo.size
    );
    let icon = new phaser.GameObjects.Graphics(scene);
    icon.fillStyle(
      phaser.Display.Color.HexStringToColor(objInfo.color).color32,
      1
    );
    icon.fillTriangleShape(triangle);
    icon.x = objInfo.location[0];
    icon.y = objInfo.location[1];
    scene.add.existing(icon);
    this.renderedObjects.push(icon);

    this.infoStruct = { ...objInfo };
    this.infoStruct.type = this.infoStruct.type.toLowerCase();
    this.scene = scene;
    //scene.add.existing(this.renderedObject);
    scene.allObjDict[String(this.infoStruct.ID)] = this;
  }
  canReceive(fromID) {
    return false;
  }
  canSend(toID) {
    let ind = this.infoStruct.sendsTo.findIndex(element => {
      return element == toID;
    });
    return ind >= 0;
  }
  sendTo(toID) {
    return this.infoStruct.ID;
  }
  receiveFrom(fromID, objID) {}
  updateRenderedState() {}
  updateGameobjectState() {}
  destroy() {
    for (let ind in this.renderedObjects) {
      this.renderedObjects[ind].destroy();
    }
  }
  getStateChanged() {
    return true;
  }
  isAnimating() {
    return false;
  }
}

export { GapFiller };
