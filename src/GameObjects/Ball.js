import phaser from "phaser";
import { GlobalSettings } from "../Globals";

class Ball {
  getType() {
    return this.infoStruct.type;
  }
  getID() {
    return this.infoStruct.ID;
  }
  constructor(scene, objInfo) {
    let ball = new phaser.GameObjects.Graphics(scene);

    let fillColor = phaser.Display.Color.HexStringToColor(objInfo.color);
    let textColor = fillColor.clone().darken(70);

    let ballShape = new phaser.Geom.Circle(0, 0, objInfo.radius);
    ball.fillStyle(fillColor.color32, 1);
    ball.fillCircleShape(ballShape);

    let ballText = new phaser.GameObjects.Text(scene, 0, 0, objInfo.text, {
      fontFamily: GlobalSettings.Font,
      fill: textColor.rgba,
      align: "center",
      fontSize: String(objInfo.radius * 1.5) + "px",
    }).setOrigin(0.5);

    this.renderedObject = new phaser.GameObjects.Container(scene);
    this.renderedObject.add(ball);
    this.renderedObject.add(ballText);
    this.renderedObject.depth = 1;
    this.renderedObject.x = objInfo.location[0];
    this.renderedObject.y = objInfo.location[1];
    this.renderedObject.angle = objInfo.rotation;
    this.isAnimatingVar = false;

    this.infoStruct = { ...objInfo };
    this.infoStruct.type = this.infoStruct.type.toLowerCase();
    this.scene = scene;
    scene.add.existing(this.renderedObject);
    scene.allObjDict[String(this.infoStruct.ID)] = this;
  }
  updateRenderedState() {
    return;
  }
  setPosition(x, y, angle) {
    this.renderedObject.x = x;
    this.renderedObject.y = y;
    this.renderedObject.angle = angle;
  }

  getPosition() {
    return {
      x: this.renderedObject.x,
      y: this.renderedObject.y,
      angle: this.renderedObject.angle,
    };
  }
  addToScene() {
    this.scene.add.existing(this.renderedObject);
  }
  animatePosition(x, y, angle, onComplete = null) {
    if (
      x == this.renderedObject.x &&
      y == this.renderedObject.y &&
      angle == this.renderedObject.angle
    )
      return;
    this.isAnimatingVar = true;
    let diff = (angle - this.renderedObject.angle + 360) % 360;
    let dir = Math.sign(diff);
    let absDiff = Math.abs(diff);
    if (absDiff > 180) {
      dir = -dir;
      absDiff = 360 - absDiff;
    }
    let newAngle = this.renderedObject.angle + dir * absDiff;
    this.scene.tweens.add({
      targets: this.renderedObject,
      x: x,
      y: y,
      angle: newAngle,
      duration: GlobalSettings.BallAnimationTime,
      ease: "Quart.easeIn",
      onComplete: () => {
        if (onComplete) onComplete();
        this.scene.sound.play("ballFall");
        this.isAnimatingVar = false;
      },
    });
  }
  canReceive(fromID) {
    return false;
  }
  canSend(toID) {
    return false;
  }
  sendTo(toID) {}
  receiveFrom(fromID, objID) {}
  updateRenderedState() {}
  updateGameobjectState() {}
  destroy() {
    this.renderedObject.destroy();
  }
  getStateChanged() {
    return false;
  }
  isAnimating() {
    return this.isAnimatingVar;
  }
}

export { Ball };
