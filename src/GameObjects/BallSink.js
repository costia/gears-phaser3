import phaser from "phaser";
import { GlobalSettings } from "../Globals";

class BallSink {
  getType() {
    return this.infoStruct.type;
  }
  getID() {
    return this.infoStruct.ID;
  }
  constructor(scene, objInfo) {
    this.text = null;
    let sink = new phaser.GameObjects.Graphics(scene);

    var rect = new Phaser.Geom.Rectangle(
      -objInfo.width / 2,
      -objInfo.height / 2,
      objInfo.width,
      objInfo.height
    );
    sink.fillStyle(
      phaser.Display.Color.HexStringToColor(objInfo.color).color32,
      1
    );
    sink.fillRectShape(rect);

    this.renderedObject = sink;
    this.renderedObject.x = objInfo.location[0];
    this.renderedObject.y = objInfo.location[1];
    this.renderedObject.angle = objInfo.rotation;

    if (objInfo.destroy) {
      this.text = new phaser.GameObjects.Image(
        scene,
        0,
        0,
        "fireSprite"
      ).setOrigin(0.5);
      this.text.x = objInfo.location[0];
      this.text.y = objInfo.location[1];
      this.text.depth = 1;
      scene.add.existing(this.text);
    }

    this.ballsList = [];
    this.stateChanged = false;
    this.isBallAnimating = false;
    this.isDoneAnimating = false;

    this.infoStruct = { ...objInfo };
    this.infoStruct.type = this.infoStruct.type.toLowerCase();
    this.scene = scene;
    scene.add.existing(this.renderedObject);
    scene.allObjDict[String(this.infoStruct.ID)] = this;
  }
  canReceive(fromID) {
    let isInList = false;
    for (let ind in this.infoStruct.receivesFrom) {
      if (fromID == this.infoStruct.receivesFrom[ind]) {
        isInList = true;
        break;
      }
    }
    return isInList && this.ballsList.length == 0;
  }
  canSend(toID) {
    return false;
  }
  sendTo(toID) {
    throw "sink obj can't send items";
  }
  receiveFrom(fromID, objID) {
    if (!this.canReceive(fromID)) {
      throw "sink " +
        String(this.infoStruct.ID) +
        " cant receive from" +
        String(toID);
    }
    this.stateChanged = true;
    this.ballsList.push(objID);
    this.isBallAnimating = true;
  }
  updateRenderedState() {
    for (let ind in this.ballsList) {
      let ballID = this.ballsList[ind];
      let ballObj = this.scene.allObjDict[ballID];
      if (ballObj) {
        ballObj.animatePosition(
          this.infoStruct.location[0],
          this.infoStruct.location[1],
          ballObj.getPosition().angle,
          () => {
            this.isBallAnimating = false;
            this.isDoneAnimating = true;
          }
        );
      }
    }
    this.stateChanged = false;
  }
  updateGameobjectState() {
    if (this.infoStruct.destroy && this.isDoneAnimating) {
      this.isDoneAnimating = false;
      this.scene.sound.play("ballFire");
      for (let ind in this.ballsList) {
        let ballID = this.ballsList[ind];
        let objRef = this.scene.allObjDict[ballID];
        delete this.scene.allObjDict[ballID];
        objRef.destroy();
      }
    }
  }
  destroy() {
    this.renderedObject.destroy();
    if (this.text) {
      this.text.destroy();
    }
  }
  getStateChanged() {
    return this.stateChanged;
  }
  isAnimating() {
    return this.isBallAnimating;
  }
}

export { BallSink };
