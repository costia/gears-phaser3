import phaser from "phaser";
import { GlobalSettings } from "../Globals";

class Tunnel {
  getType() {
    return this.infoStruct.type;
  }
  getID() {
    return this.infoStruct.ID;
  }
  constructor(scene, objInfo) {
    this.renderedObjects = [];
    let circle = new Phaser.Geom.Circle(0, 0, objInfo.radius);
    let fillColor = phaser.Display.Color.HexStringToColor(objInfo.color);
    let textColor = phaser.Display.Color.HexStringToColor(objInfo.color).darken(
      40
    );
    let textStyle = {
      fontSize: String(objInfo.radius * 1.5) + "px",
      fill: textColor.rgba,
      fontFamily: GlobalSettings.Font,
      align: "center",
    };

    let v2 = new phaser.Math.Vector2(
      objInfo.end.location[0] - objInfo.start.location[0],
      objInfo.end.location[1] - objInfo.start.location[1]
    ).normalize();
    const connectorOutlineLine = new Phaser.Geom.Line(
      objInfo.start.location[0] + v2.x * objInfo.radius,
      objInfo.start.location[1] + v2.y * objInfo.radius,
      objInfo.end.location[0] - v2.x * objInfo.radius,
      objInfo.end.location[1] - v2.y * objInfo.radius
    );
    let connectorOutline = scene.add.graphics();
    connectorOutline.lineStyle(objInfo.radius * 2 + 6, textColor.color32, 1);
    connectorOutline.strokeLineShape(connectorOutlineLine);
    this.renderedObjects.push(connectorOutline);

    const connectorLine = new Phaser.Geom.Line(
      objInfo.start.location[0],
      objInfo.start.location[1],
      objInfo.end.location[0],
      objInfo.end.location[1]
    );
    let connector = scene.add.graphics();
    connector.lineStyle(objInfo.radius * 2, fillColor.color32, 1);
    connector.strokeLineShape(connectorLine);
    this.renderedObjects.push(connector);

    let start = new phaser.GameObjects.Graphics(scene);
    start.fillStyle(fillColor.color32, 1);
    start.fillCircleShape(circle);
    start.x = objInfo.start.location[0];
    start.y = objInfo.start.location[1];
    start.angle = objInfo.start.rotation;
    this.renderedObjects.push(start);
    scene.add.existing(start);

    let startText = scene.add
      .text(
        objInfo.start.location[0],
        objInfo.start.location[1],
        "↑",
        textStyle
      )
      .setOrigin(0.5);
    startText.angle =
      90 +
      phaser.Math.RadToDeg(
        phaser.Math.Angle.Between(
          objInfo.start.location[0],
          objInfo.start.location[1],
          objInfo.end.location[0],
          objInfo.end.location[1]
        )
      );
    this.renderedObjects.push(startText);

    let end = new phaser.GameObjects.Graphics(scene);
    end.fillStyle(fillColor.color32, 1);
    end.fillCircleShape(circle);
    end.x = objInfo.end.location[0];
    end.y = objInfo.end.location[1];
    end.angle = objInfo.end.rotation;
    this.renderedObjects.push(end);
    scene.add.existing(end);

    let endText = scene.add
      .text(objInfo.end.location[0], objInfo.end.location[1], "↑", textStyle)
      .setOrigin(0.5);
    endText.angle =
      90 +
      phaser.Math.RadToDeg(
        phaser.Math.Angle.Between(
          objInfo.start.location[0],
          objInfo.start.location[1],
          objInfo.end.location[0],
          objInfo.end.location[1]
        )
      );
    this.renderedObjects.push(endText);

    this.ballsAtStart = [];
    this.ballsAtEnd = [];
    this.stateChanged = false;

    this.isReceiveAnimating = false;
    this.isReceiveDone = false;
    this.isTransitionAnimating = false;
    this.isTransitionDone = false;

    this.infoStruct = { ...objInfo };
    this.infoStruct.type = this.infoStruct.type.toLowerCase();
    this.scene = scene;
    scene.allObjDict[String(this.infoStruct.ID)] = this;
  }
  canReceive(fromID) {
    let ind = this.infoStruct.receivesFrom.findIndex(element => {
      return element == fromID;
    });
    return (
      !this.isAnimating() &&
      this.ballsAtEnd.length == 0 &&
      this.ballsAtStart.length == 0 &&
      ind >= 0
    );
  }
  canSend(toID) {
    let ind = this.infoStruct.sendsTo.findIndex(element => {
      return element == toID;
    });
    return !this.isAnimating() && this.ballsAtEnd.length > 0 && ind >= 0;
  }
  sendTo(toID) {
    return this.ballsAtEnd.pop();
  }
  receiveFrom(fromID, objID) {
    this.ballsAtStart.push(objID);
    let ballObj = this.scene.allObjDict[objID];
    this.isReceiveAnimating = true;
    ballObj.animatePosition(
      this.infoStruct.start.location[0],
      this.infoStruct.start.location[1],
      this.infoStruct.start.rotation,
      () => {
        this.isReceiveDone = true;
        this.isReceiveAnimating = false;
      }
    );
  }
  updateRenderedState() {}
  updateGameobjectState() {
    if (this.isReceiveDone) {
      this.isReceiveDone = false;
      let ballID = this.ballsAtStart.pop();
      let ballObj = this.scene.allObjDict[ballID];
      this.ballsAtEnd.push(ballID);
      this.isTransitionAnimating = true;
      ballObj.animatePosition(
        this.infoStruct.end.location[0],
        this.infoStruct.end.location[1],
        this.infoStruct.end.rotation,
        () => {
          this.isTransitionDone = true;
          this.isTransitionAnimating = false;
        }
      );
    }
  }
  destroy() {
    for (let ind in this.renderedObjects) {
      this.renderedObjects[ind].destroy();
    }
  }
  getStateChanged() {
    return true;
  }
  isAnimating() {
    return this.isReceiveAnimating || this.isTransitionAnimating;
  }
}

export { Tunnel };
