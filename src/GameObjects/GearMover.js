import phaser from "phaser";

class GearMover {
  getType() {
    return this.infoStruct.type;
  }
  getID() {
    return this.infoStruct.ID;
  }
  constructor(scene, objInfo) {
    this.renderedObjects = [];
    this.stateChangeRequested = false;

    let path = new phaser.GameObjects.Graphics(scene);
    path.fillStyle(
      phaser.Display.Color.HexStringToColor(objInfo.color).color32,
      1
    );
    path.lineStyle(
      7,
      phaser.Display.Color.HexStringToColor(objInfo.color).color32,
      1
    );

    for (let ind in objInfo.states) {
      path.fillCircleShape(
        new phaser.Geom.Circle(
          objInfo.states[ind].location[0],
          objInfo.states[ind].location[1],
          7
        )
      );
    }
    for (let ind = 1; ind < objInfo.states.length; ind++) {
      let connectorLine = new Phaser.Geom.Line(
        objInfo.states[ind - 1].location[0],
        objInfo.states[ind - 1].location[1],
        objInfo.states[ind].location[0],
        objInfo.states[ind].location[1]
      );
      path.strokeLineShape(connectorLine);
    }
    path.depth = -1;
    scene.add.existing(path);
    this.renderedObjects.push(path);

    this.button = new phaser.GameObjects.Graphics(scene);
    let buttonShape = new phaser.Geom.Circle(0, 0, objInfo.radius);
    this.button.fillStyle(
      phaser.Display.Color.HexStringToColor(objInfo.buttonColor).color32,
      1
    );
    this.button.fillCircleShape(buttonShape);

    this.button.setInteractive(buttonShape, (shape, x, y, obj) => {
      return phaser.Geom.Circle.Contains(shape, x, y);
    });
    this.button.on("pointerdown", (pointer, gameObject) => {
      this.stateChangeRequested = true;
    });
    scene.add.existing(this.button);
    this.renderedObjects.push(this.button);

    this.buttonAttached = false;

    this.gearState = 0;

    this.infoStruct = { ...objInfo };
    this.infoStruct.type = this.infoStruct.type.toLowerCase();
    this.scene = scene;

    scene.allObjDict[String(this.infoStruct.ID)] = this;
  }
  canReceive(fromID) {
    return false;
  }
  canSend(toID) {
    return false;
  }
  sendTo(toID) {}
  receiveFrom(fromID, objID) {}
  updateRenderedState() {}
  updateGameobjectState() {
    if (!this.buttonAttached) {
      this.buttonAttached = true;
      let gearID = this.infoStruct.gearID;
      let gearObj = this.scene.allObjDict[gearID];
      gearObj.attachRenderObject(this.button);

      gearObj.setGearData(this.infoStruct.states[this.gearState]);
    }
    if (this.stateChangeRequested) {
      this.scene.sound.play("gearMove");
      this.scene.Moves++;
      this.stateChangeRequested = false;
      this.gearState = (this.gearState + 1) % this.infoStruct.states.length;
      let gearID = this.infoStruct.gearID;
      let gearObj = this.scene.allObjDict[gearID];
      gearObj.setGearData(this.infoStruct.states[this.gearState]);
    }
  }
  destroy() {
    for (let ind in this.renderedObjects) {
      this.renderedObjects[ind].destroy();
    }
  }
  getStateChanged() {
    return true;
  }
  isAnimating() {
    return false;
  }
}

export { GearMover };
