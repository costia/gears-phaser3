import phaser from "phaser";
import { GlobalSettings } from "../Globals";

class Gear {
  getType() {
    return this.infoStruct.type;
  }
  getID() {
    return this.infoStruct.ID;
  }
  createGearSprite(scene, objInfo) {
    let gear = new phaser.GameObjects.Graphics(scene);
    let bgColor = phaser.Display.Color.HexStringToColor(objInfo.color);
    bgColor = bgColor.brighten(20).desaturate(40);
    gear.fillStyle(bgColor.color32, 1);
    gear.fillCircleShape(
      new phaser.Geom.Circle(
        objInfo.largeRadius,
        objInfo.largeRadius,
        objInfo.largeRadius
      )
    );

    gear.fillStyle(
      phaser.Display.Color.HexStringToColor(objInfo.color).color32,
      1
    );
    gear.fillCircleShape(
      new phaser.Geom.Circle(
        objInfo.largeRadius,
        objInfo.largeRadius,
        objInfo.smallRadius
      )
    );
    for (let ind in objInfo.prongs) {
      if (objInfo.prongs[ind]) {
        gear.beginPath();
        let radius = objInfo.largeRadius - 2;
        gear.moveTo(objInfo.largeRadius, objInfo.largeRadius);
        gear.slice(
          objInfo.largeRadius,
          objInfo.largeRadius,
          radius,
          Phaser.Math.DegToRad(objInfo.sectionAngle * ind + 1),
          Phaser.Math.DegToRad(objInfo.sectionAngle * (ind - 1)),
          true
        );
        gear.closePath();
        gear.fillPath();
      }
    }

    let pinRadius = Math.max(5, objInfo.smallRadius / 10);
    let pinColor = phaser.Display.Color.HexStringToColor(objInfo.color);
    pinColor = pinColor.darken(30);
    gear.fillStyle(pinColor.color32, 1);
    gear.fillCircleShape(
      new phaser.Geom.Circle(
        objInfo.largeRadius,
        objInfo.largeRadius,
        pinRadius
      )
    );
    gear.generateTexture(
      "gearTexture_" + String(objInfo.ID),
      objInfo.largeRadius * 2,
      objInfo.largeRadius * 2
    );
    gear.destroy();
  }

  createOutline(scene, objInfo) {
    let sendReceive = [];
    for (let ind = 0; ind < 360 / objInfo.sectionAngle; ind++) {
      sendReceive.push(
        objInfo.sendsTo[ind] >= 0 || objInfo.receivesFrom[ind] >= 0
      );
    }
    let outlineColor = phaser.Display.Color.HexStringToColor(objInfo.color);
    outlineColor = outlineColor.darken(30);
    this.outline = new phaser.GameObjects.Graphics(scene);
    this.outline.fillStyle(outlineColor.color32, 1);
    this.outline.lineStyle(5, outlineColor.color32, 1);
    for (let ind in sendReceive) {
      if (!sendReceive[ind]) {
        this.outline.beginPath();
        let radius = objInfo.largeRadius + 2.5;

        this.outline.arc(
          0,
          0,
          radius,
          Phaser.Math.DegToRad(objInfo.sectionAngle * ind + 1),
          Phaser.Math.DegToRad(objInfo.sectionAngle * (ind - 1)),
          true
        );
        this.outline.strokePath();
      }
    }
    this.outline.x = objInfo.location[0];
    this.outline.y = objInfo.location[1];
    scene.add.existing(this.outline);
  }

  constructor(scene, objInfo) {
    this.createOutline(scene, objInfo);
    this.createGearSprite(scene, objInfo);

    let gearSprite = new phaser.GameObjects.Sprite(
      scene,
      0,
      0,
      "gearTexture_" + String(objInfo.ID)
    ).setOrigin(0.5);

    this.renderedObject = new phaser.GameObjects.Container(scene);
    this.renderedObject.add(gearSprite);
    this.renderedObject.x = objInfo.location[0];
    this.renderedObject.y = objInfo.location[1];
    this.renderedObject.angle = 0;
    this.renderedObject.setInteractive(
      new phaser.Geom.Circle(0, 0, objInfo.largeRadius),
      (shape, x, y, obj) => {
        return phaser.Geom.Circle.Contains(shape, x, y);
      }
    );

    this.prevAngle = 0;
    this.newMouseAngle = 0;
    this.prevMouseAngle = 0;
    this.rotationState = 0;
    this.rotationChanged = false;

    this.totalSteps = 360 / objInfo.sectionAngle;
    this.isGearAnimating = false;
    this.gearDoneAnimating = false;
    this.isBallAnimating = false;

    this.receivedBalls = [];

    this.ballsList = Array.apply(null, {
      length: this.totalSteps,
    }).map(function() {
      return null;
    });
    this.ballsTimestamp = Array.apply(null, {
      length: this.totalSteps,
    }).map(function() {
      return -1;
    });
    this.stateChanged = false;

    this.infoStruct = { ...objInfo };
    this.infoStruct.type = this.infoStruct.type.toLowerCase();
    this.scene = scene;
    scene.add.existing(this.renderedObject);
    scene.allObjDict[String(this.infoStruct.ID)] = this;

    this.initEventListeners();
  }
  canReceive(fromID) {
    //if (this.isAnimating()) return false;
    let ind = this.infoStruct.receivesFrom.findIndex(element => {
      return element == fromID;
    });
    if (ind < 0) return false;
    let indCurrentProng =
      (ind - this.rotationState + this.totalSteps) % this.totalSteps;
    return (
      this.infoStruct.prongs[indCurrentProng] == 0 &&
      this.ballsList[indCurrentProng] == null
    );
  }
  canSend(toID) {
    if (this.isAnimating()) return false;
    let ind = this.infoStruct.sendsTo.findIndex(element => {
      return element == toID;
    });
    if (ind < 0) return false;
    let indCurrentProng =
      (ind - this.rotationState + this.totalSteps) % this.totalSteps;
    return (
      this.infoStruct.prongs[indCurrentProng] == 0 &&
      this.ballsList[indCurrentProng] != null &&
      this.ballsTimestamp[indCurrentProng] != this.scene.Moves
    );
  }
  sendTo(toID) {
    if (!this.canSend(toID)) {
      throw "gear " +
        String(this.infoStruct.ID) +
        " cant send to" +
        String(toID);
    }
    let ind = this.infoStruct.sendsTo.findIndex(element => {
      return element == toID;
    });
    let indCurrentProng =
      (ind - this.rotationState + this.totalSteps) % this.totalSteps;
    let ballID = this.ballsList[indCurrentProng];
    this.ballsList[indCurrentProng] = null;
    this.ballsTimestamp[indCurrentProng] = -1;
    this.stateChanged = true;

    // release ball from gear container
    // change ball coordinates from
    // gear-local coords to world coords
    let ballObj = this.scene.allObjDict[ballID];
    this.renderedObject.remove(ballObj.renderedObject);
    let angle = phaser.Math.DegToRad(
      (indCurrentProng + this.rotationState - 0.5) *
        this.infoStruct.sectionAngle
    );
    let radius =
      (this.infoStruct.largeRadius + this.infoStruct.smallRadius) / 2;
    ballObj.setPosition(
      radius * Math.cos(angle) + this.infoStruct.location[0],
      radius * Math.sin(angle) + this.infoStruct.location[1],
      ballObj.getPosition().angle +
        this.rotationState * this.infoStruct.sectionAngle
    );

    ballObj.addToScene();

    return ballID;
  }
  receiveFrom(fromID, objID) {
    if (!this.canReceive(fromID)) {
      throw "gear " +
        String(this.infoStruct.ID) +
        " cant receive from" +
        String(toID);
    }
    let ind = this.infoStruct.receivesFrom.findIndex(element => {
      return element == fromID;
    });
    let indCurrentProng =
      (ind - this.rotationState + this.totalSteps) % this.totalSteps;
    let objRef = this.scene.allObjDict[objID];
    if (objRef.infoStruct.type == "ball") {
      this.ballsList[indCurrentProng] = objID;
      this.ballsTimestamp[indCurrentProng] = this.scene.Moves;
      this.receivedBalls.push(objID);
    } else if (objRef.infoStruct.type == "gapfiller") {
      this.infoStruct.prongs[indCurrentProng] = 1;
      this.createGearSprite(this.scene, this.infoStruct);
    }
    this.stateChanged = true;
  }

  animateRotation(angle, onComplete) {
    this.scene.sound.play("gearClick");
    this.scene.tweens.add({
      targets: this.renderedObject,
      angle: angle,
      duration: GlobalSettings.GearAnimationTime,
      ease: "Power2",
      onComplete: () => {
        if (onComplete) onComplete();
      },
    });
  }

  updateRenderedState() {
    // update gear rotation
    if (this.rotationChanged) {
      this.rotationChanged = false;
      let newAngle = this.rotationState * this.infoStruct.sectionAngle;
      let diff = (newAngle - this.renderedObject.angle + 360) % 360;
      let dir = Math.sign(diff);
      let absDiff = Math.abs(diff);
      if (absDiff > 180) {
        dir = -dir;
        absDiff = 360 - absDiff;
      }
      newAngle = this.renderedObject.angle + dir * absDiff;
      this.animateRotation(newAngle, () => {
        this.isGearAnimating = false;
        this.gearDoneAnimating = true;
      });
    }

    for (let ind in this.receivedBalls) {
      let objID = this.receivedBalls[ind];
      let indCurrentProng = this.ballsList.findIndex(element => {
        return element == objID;
      });
      //after animation of arriving to gear ends
      // add ball to gear's container
      // and set coordinates to gear-local
      let onComplete = () => {
        let ballObj = this.scene.allObjDict[objID];

        let radius =
          (this.infoStruct.largeRadius + this.infoStruct.smallRadius) / 2;
        let angle = phaser.Math.DegToRad(
          this.infoStruct.sectionAngle / 2 -
            indCurrentProng * this.infoStruct.sectionAngle
        );
        ballObj.setPosition(
          Math.cos(angle) * radius,
          -Math.sin(angle) * radius,
          ballObj.getPosition().angle +
            -this.rotationState * this.infoStruct.sectionAngle
        );
        this.renderedObject.add(ballObj.renderedObject);
        this.isBallAnimating = false;
      };

      // animate ball arriving from source
      // the call onComplete to parent ball to gear
      let ballObj = this.scene.allObjDict[objID];
      let angle = phaser.Math.DegToRad(
        (indCurrentProng + this.rotationState - 0.5) *
          this.infoStruct.sectionAngle
      );
      let radius =
        (this.infoStruct.largeRadius + this.infoStruct.smallRadius) / 2;
      this.isBallAnimating = true;
      ballObj.animatePosition(
        radius * Math.cos(angle) + this.infoStruct.location[0],
        radius * Math.sin(angle) + this.infoStruct.location[1],
        ballObj.getPosition().angle,
        onComplete
      );
    }
    this.receivedBalls = [];
    this.stateChanged = false;
  }

  updateGameobjectState() {
    let steps = 0;
    let dir = 1;
    if (this.buttonDown) {
      let diff = this.newMouseAngle - this.prevMouseAngle;
      dir = Math.sign(diff);
      let amount = Math.abs(diff);
      if (amount > 180) {
        dir = -dir;
        amount = 360 - amount;
      }

      steps = Math.round(amount / this.infoStruct.sectionAngle);
      amount -= steps * this.infoStruct.sectionAngle;
      this.prevMouseAngle += steps * dir * this.infoStruct.sectionAngle;
      this.prevMouseAngle = this.prevMouseAngle % 360;
      if (this.prevMouseAngle < 0) this.prevMouseAngle += 360;
    }

    // after gear animation done
    // allow 1 update iteration to
    // check send/recieve at new position
    // before allowing more rotaion changes
    if (this.gearDoneAnimating) {
      this.gearDoneAnimating = false;
      this.stateChanged = true;
    } else if (steps > 0 && !this.isAnimating()) {
      this.rotationState += dir;
      if (this.rotationState >= this.totalSteps) {
        this.rotationState -= this.totalSteps;
      } else if (this.rotationState < 0) {
        this.rotationState += this.totalSteps;
      }
      this.stateChanged = true;
      this.isGearAnimating = true;
      this.scene.Moves += steps;
      this.rotationChanged = true;
    }
  }

  attachRenderObject(obj) {
    this.renderedObject.add(obj);
    this.stateChanged = true;
  }

  setGearData(state, callback) {
    this.infoStruct.receivesFrom = [...state.receivesFrom];
    this.infoStruct.sendsTo = [...state.sendsTo];

    let oldOutline = this.outline;
    this.createOutline(this.scene, this.infoStruct);
    oldOutline.destroy();

    this.isGearAnimating = true;

    // this.renderedObject.x = state.location[0];
    // this.renderedObject.y = state.location[1];

    this.scene.tweens.add({
      targets: this.renderedObject,
      x: state.location[0],
      y: state.location[1],
      duration: GlobalSettings.GearAnimationTime,
      ease: "Power2",
      onComplete: () => {
        this.isGearAnimating = false;
        this.gearDoneAnimating = true;
        this.infoStruct.location = [...state.location];
        if (callback) callback();
      },
    });

    this.scene.tweens.add({
      targets: this.outline,
      x: state.location[0],
      y: state.location[1],
      duration: GlobalSettings.GearAnimationTime,
      ease: "Power2",
    });

    this.stateChanged = true;
  }

  initEventListeners() {
    this.buttonDown = false;
    this.prevMouseAngle = 0;
    this.newMouseAngle = 0;

    this.renderedObject.on("pointerdown", (pointer, gameObject) => {
      this.buttonDown = true;
      this.prevMouseAngle = phaser.Math.Angle.Between(
        pointer.position.x,
        pointer.position.y,
        this.infoStruct.location[0],
        this.infoStruct.location[1]
      );
      this.prevMouseAngle =
        (phaser.Math.RadToDeg(this.prevMouseAngle) + 360) % 360;
      this.newMouseAngle = this.prevMouseAngle;
    });
    this.scene.input.on("pointerup", pointer => {
      this.buttonDown = false;
    });
    this.scene.input.on("pointerupoutside", pointer => {
      this.buttonDown = false;
    });

    this.scene.input.on("pointermove", pointer => {
      if (this.buttonDown) {
        this.newMouseAngle = phaser.Math.Angle.Between(
          pointer.position.x,
          pointer.position.y,
          this.infoStruct.location[0],
          this.infoStruct.location[1]
        );
        this.newMouseAngle =
          (phaser.Math.RadToDeg(this.newMouseAngle) + 360) % 360;
      }
    });
  }
  destroy() {
    this.renderedObject.destroy();
    this.outline.destroy();
  }
  getStateChanged() {
    return this.stateChanged;
  }
  isAnimating() {
    return this.isGearAnimating || this.isBallAnimating;
  }
}

export { Gear };
